# Geant4_GPU
Geant4_GPU is a project aimed at reducing the simulation time required by the Geant4 toolkit, which is focused on hadronic simulations. The optimization is carried out using a heterogeneous computing model with a graphics processing unit (GPU) acting as a co-processor during simulation execution. The GPU kernels are included in Geant4 as an external module, so the simulation design will not change.

__Index__

[toc]

### Test enviroment
The computing system used in this work has the following specifications:

* CPU: Intel i7-6700 CPU - 3.40GHz
* GPU: GeForce GTX 750 Ti - Capability 5.0 (Maxwell)
* geant4-10.5.p01 / geant4-10.6.p02
* CUDA v.10.2
* Debian 10 (Buster)


<br />
<br />

# Installation
<!--- The installation could be completed as usual with geant4, including the installation of cuda from Nvidia as a prerequisite. --->
The geant4 installation could proceed as usual, including the installation of cuda from Nvidia as a prerequisite. To check more details about the geant4 installation refer to [Installaion Guide](https://geant4-userdoc.web.cern.ch/UsersGuides/InstallationGuide/html/) and for specific information about the cuda installation in any linux system refer to [Installation Guide CUDA](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html).

The next steps consider the previous installation of the geatn4 requirements and those commads are for _Debian_.

1.    __Install nvidia drivers__ following the steps described in [drivers installaion guide](https://gitlab.com/camolinas/geant4_gpu/-/blob/master/Documents/drivers_nvidia.pdf) or go directly to the officiall side [Installation Guide CUDA](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html).

2.    __Install CUDA__.
      1.    __Install packages__
      ```console
      $ sudo apt-get install nvidia-cuda-dev nvidia-cuda-toolkit
      ```
      2.    __Verify CUDA version__
      ```console
      $​ dpkg -l | grep cuda-toolkit
      ```
3.    __Install geant4__

      1. 
      ```console   
      $ cmake -DGEANT4_INSTALL_DATA=ON -DGEANT4_INSTALL_DATADIR=/HOME_DIRECTORY/geant4_gpu/code/Geant4-10.5_cpu/data    -DCMAKE_INSTALL_PREFIX=/HOME_DIRECTORY/geant4_gpu/code/Geant4-10.5_cpu/install /HOME_DIRECTORY/geant4_gpu/code/Geant4-10.5_cpu/src/geant4.10.05.p01
      ```

      2. 
      ```console   
      $ cmake -DGeant4_DIR=/HOME_DIRECTORY/geant4_gpu/code/Geant4-10.5_cpu/install/lib/Geant4-10.5.1 ../
      ```
      3.    __Load State Variables__
      ```console
      $ source /HOME_DIRECTORY/geant4_gpu/code/Geant4-10.5_gpu/install/bin/geant4.sh
      ```

<br />
<br />

# Results
Four GPU kernels were tested with Hadronic simulations using modified versions of the Geant4 distribution with the cuda external module. More specifically, Geant4-10.5 p01 was the distributions employed, and
the simulation used was the g4pntest v1.

The GPU kernels were 97% faster than CPU routines, but when data transfer time was taken into account, the speed-up was barely 10%. However, porting larger Geant4 routines to the GPU would reduce the data transfer effect on the execution time.
<br />
<br />


<img src="./Documents/Images/t1_general_table.png" alt="drawing" height="70%" width="70%"/>

<br />
<br />

<img src="./Documents/Images/t2_function_table.png" alt="drawing" height="40%" width="40%"/>

<br />
<br />


<img src="./Documents/Images/t3_kernel_table.png" alt="drawing" height="40%" width="40%"/>


<br />
<br />

# Git folders

* __(1)__ Geant4_GPU
    * __(1.1)__ Documents
        * __(1.1.1)__ Resumen_GPU.pdf &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Geant4_GPU project summary
        * __(1.1.2)__ Estado_Arte.pdf &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Related projects - Geant4 implement in GPU                                     
        * __(1.1.3)__ drivers_nvidia.pdf &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; // Installation Guide
        * __(1.1.4)__ related_papers/ &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; // Papers with similar purpose and structure
        * __(1.1.5)__ notes/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Some developing notes
    * __(1.2)__ code
        * __(1.2.1)__ cuda_funtions
            * __(1.2.1.1)__ cuda_check_times &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Folder with necessary code to implement G4ParticleHPVector::Check/Times routines
            * __(1.2.1.2)__ cuda_harmo2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Folder with necessary code to implement G4ParticleHPDatadaPoint::Harmonise routine
            * __(1.2.1.3)__ cuda_times &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Folder with necessary code to implement G4ParticleHPVector::Times routine
            * __(1.2.1.3)__ cuda_getxsec &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Folder with necessary code to implement G4ParticleHPVector::GetXsec routine
        * __(1.2.2)__ Geant4-10.5_gpu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Geant4 distribution version 10.5 
            * __(1.2.2.1)__ src &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Source code with cuda module incorporate (Operative)
            * __(1.2.2.2)__ examples_aps &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Simulations and .txt files with execution debugging data
    * __(1.3)__ README.md &nbsp; &nbsp;&nbsp;&nbsp; // (You are here)

  








