// -------------------------------------------------------------------
//
// GEANT4 CUDA header file
//
//
// File name:    cuda_fill_nullptr
//
// Author  Dorfell Parra, 2020/07/21
//
// Modifications:
//
// Description:
// This is a GPU kernel used to fill a data vector with null pointers.
// Description - E

class G4PhysicsVector;

int cuda_fill_nullptr(const G4int MAXZEL, G4PhysicsVector* data[] );
