// -------------------------------------------------------------------
//
// GEANT4 CUDA file
//
//
// File name:    cuda_hello
//
// Author  Dorfell Parra, 2020/07/21
//
// Modifications:
//
// Description:
// This is a GPU kernel used to print Hello from the CUDA module..
// Description - E


#include <cuda_runtime.h>

#include <cuda_runtime.h>
#include <iostream>
#include <memory>
#include <string>

// CUDA-C includes
#include <cuda.h>

/* GPU Kernel */
__global__ void
vectorAdd(const float *A, const float *B, float *C, int numElements){
  int i = blockDim.x * blockIdx.x + threadIdx.x;

  if (i < numElements){
    C[i] = A[i] + B[i]; }
}


////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int myfoo_cuda(void) {

  printf("CUDA Device Query (Runtime API) version (CUDART static linking)\n\n");

  int deviceCount = 0;
  cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

  // This function call returns 0 if there are no CUDA capable devices.
  if (deviceCount == 0) {
    printf("There are no available device(s) that support CUDA\n");
  } else {
    printf("Detected %d CUDA Capable device(s)\n", deviceCount);
  }

  int dev, driverVersion = 0, runtimeVersion = 0;

  for (dev = 0; dev < deviceCount; ++dev) {
    cudaSetDevice(dev);
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);

    printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);

    // Console log
    cudaDriverGetVersion(&driverVersion);
    cudaRuntimeGetVersion(&runtimeVersion);
    printf("  CUDA Driver Version / Runtime Version          %d.%d / %d.%d\n",
           driverVersion / 1000, (driverVersion % 100) / 10,
           runtimeVersion / 1000, (runtimeVersion % 100) / 10);
    printf("  CUDA Capability Major/Minor version number:    %d.%d\n",
           deviceProp.major, deviceProp.minor);
  }

  // Print the vector length to be used, and compute its size
  int numElements = 50000;
  size_t size = numElements * sizeof(float);
  printf("[Vector addition of %d elements]\n", numElements);
 
  // Allocate the host input vector A, B, C
  float *h_A = (float *)malloc(size);
  float *h_B = (float *)malloc(size);
  float *h_C = (float *)malloc(size);

  // Initialize the host input vectors
  for (int i = 0; i < numElements; ++i){
    h_A[i] = rand()/(float)RAND_MAX;
    h_B[i] = rand()/(float)RAND_MAX;  }

  // Allocate the device input vector A,B,C
  float *d_A = NULL;
  float *d_B = NULL;
  float *d_C = NULL;

  // Copy the host input vectors A and B in host memory to the device input vectors 
  // in device memory
  printf("Copy input data from the host memory to the CUDA device\n");
  cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);

  // Launch the Vector Add CUDA Kernel
  int threadsPerBlock = 256;
  int blocksPerGrid =(numElements + threadsPerBlock - 1) / threadsPerBlock;
  printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid,threadsPerBlock);
  vectorAdd<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, numElements);

  // Copy the device result vector in device memory to the host result vector
  // in host memory.
  printf("Copy output data from the CUDA device to the host memory\n");
  cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);

   // Verify that the result vector is correct
   for (int i = 0; i < numElements; ++i) {
     if (fabs(h_A[i] + h_B[i] - h_C[i]) > 1e-5) {
       fprintf(stderr, "Result verification failed at element %d!\n", i);  }
   }
   printf("Test PASSED\n");

   // Free device global memory
   cudaFree(d_A); cudaFree(d_B); cudaFree(d_C);

  // Free host memory
  free(h_A);  free(h_B); free(h_C);

  printf("GPU Done\n");

  return 0;
}
