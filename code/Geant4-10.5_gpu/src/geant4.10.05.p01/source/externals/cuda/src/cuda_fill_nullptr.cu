// -------------------------------------------------------------------
//
// GEANT4 CUDA file
//
//
// File name:    cuda_fill_nullptr
//
// Author  Dorfell Parra, 2020/07/21
//
// Modifications:
//
// Description:
// This is a GPU kernel used to fill a data vector with null pointers.
// Description - E

// Geant4 libraries
#include "globals.hh"          // G4int definition
#include "G4PhysicsVector.hh"  // G4PhysicsVector definition

// CUDA-C includes
#include <cuda.h>

using namespace std; 


/* GPU  info prototype */
void gpu_info(void);

/* GPU Kernel */
__global__ void fill_nullptr(G4int MAXZEL, float *d_data){

  // Indexing threads
  int i = blockDim.x * blockIdx.x + threadIdx.x; 

  if (i < MAXZEL){
    d_data[i] = NULL; }
}


////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int cuda_fill_nullptr(const G4int MAXZEL, G4PhysicsVector* data[] ) {
  
  // Print GPU , CUDA info
  gpu_info();

  /* // CPU code
  for(G4int i=0; i<MAXZEL; ++i) {
    delete data[i];
    data[i] = nullptr;
    printf("Inside for cpu  %d \n", data[i]);
  } */

  // Sizes of inputs
  size_t data_size   = MAXZEL * sizeof(float);

  // Allocate the device input data
  float *d_data = NULL;

  // Copy host MAXZEL and data, to device memory
  printf("Copy input data from the host memory to the CUDA device\n");
  cudaMemcpy(d_data, data, data_size, cudaMemcpyHostToDevice);

  // Launch the Vector Add CUDA Kernel
  int threadsPerBlock = 96; // Only need MAXZEVAL = 93
  int blocksPerGrid = 1;
  printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid,threadsPerBlock);
  fill_nullptr<<<blocksPerGrid, threadsPerBlock>>>(MAXZEL, d_data);

  // Copy from the device memory to the host memory.
  printf("Copy output data from the CUDA device to the host memory\n");
  cudaMemcpy(data, d_data, data_size, cudaMemcpyDeviceToHost);

  // Free device global memory
  cudaFree(d_data);

  printf("GPU Done\n");
  return 0;
}


/* GPU  info */
void gpu_info(void){

  int deviceCount = 0;
  cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

  int dev, driverVersion = 0, runtimeVersion = 0;

  for (dev = 0; dev < deviceCount; ++dev) {
    cudaSetDevice(dev);
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);

    // Console log
    cudaDriverGetVersion(&driverVersion);
    cudaRuntimeGetVersion(&runtimeVersion);
    printf("  CUDA Driver Version / Runtime Version          %d.%d / %d.%d\n",
           driverVersion / 1000, (driverVersion % 100) / 10,
           runtimeVersion / 1000, (runtimeVersion % 100) / 10);
    printf("  CUDA Capability Major/Minor version number:    %d.%d\n",
           deviceProp.major, deviceProp.minor);
  }
}
