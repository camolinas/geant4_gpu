#! /bin/bash

# how to use: ./environment.sh

# Geant4
export G4ABLADATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4ABLA3.1
export G4ENSDFSTATEDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4ENSDFSTATE2.2
export G4INCLDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4INCL1.0
export G4LEDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4EMLOW7.9.1
export G4LEVELGAMMADATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/PhotonEvaporation5.5
export G4NEUTRONHPDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4NDL4.6
export G4PARTICLEXSDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4PARTICLEXS2.1
export G4PIIDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4PII1.3
export G4RADIOACTIVEDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/RadioactiveDecay5.4
export G4REALSURFACEDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/RealSurface2.1.1
export G4SAIDXSDATA=/home/gfnun/Geant4-10.5/install/share/Geant4-10.5.1/data/G4SAIDDATA2.0

source /home/gfnun/Geant4-10.5/install/bin/geant4.sh 
