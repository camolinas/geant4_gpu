//
// $Id: PNtest.cc July 2020 $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "SetupConstruction.hh"
#include "ActionInitialization.hh"

#include "Randomize.hh"

#include "G4EventManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

#include "QGSP_BERT_HP.hh"
#include "G4PhysListFactory.hh"
#include "G4StepLimiterPhysics.hh"
#include "G4VModularPhysicsList.hh"
#include "G4VUserActionInitialization.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include <chrono>
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
using namespace std;
 
int main(int argc,char** argv) {

auto start = chrono::high_resolution_clock::now();

  //choose the Random engine
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  //set random seed with system time
  G4long seed = time(NULL);
  G4Random::setTheSeed(seed);
  
  // G4MULTITHREADED Construct the default run manager
/* #ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
  runManager->SetNumberOfThreads(2); // Is equal to 2 by default
#else  */
  G4RunManager* runManager = new G4RunManager;
//#endif
  
  // set mandatory initialization classes
  runManager->SetUserInitialization(new SetupConstruction);
   G4VModularPhysicsList* physicsList = new QGSP_BERT_HP;
 
  physicsList->RegisterPhysics(new G4StepLimiterPhysics());
  runManager->SetUserInitialization(physicsList);

  runManager->SetUserInitialization(new ActionInitialization());

  // Initialize G4 kernel
  //runManager->Initialize();
#ifdef G4VIS_USE
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
#endif

auto s_init = chrono::high_resolution_clock::now();
   //Initialize G4 kernel
  runManager->Initialize();
auto s_init2 = chrono::high_resolution_clock::now();

  // get the pointer to the User Interface manager
  G4UImanager* UI = G4UImanager::GetUIpointer();  

  if(argc == 1)
    {
#ifdef G4UI_USE
      G4UIExecutive* ui = new G4UIExecutive(argc, argv);
#ifdef G4VIS_USE
      UI->ApplyCommand("/control/execute vis.mac");
#endif
      ui->SessionStart();
      delete ui;
#endif
    }
  // Batch mode
  else
    {
      G4String command = "/control/execute ";
      G4String fileName = argv[1];
      UI->ApplyCommand(command+fileName);
    }
  
#ifdef G4VIS_USE
  delete visManager;
#endif     
  //! @attention Free the store: user actions, physics_list and detector_description are
  //!                 owned and deleted by the run manager, so they should not
  //!                 be deleted in the main() program !
  delete runManager;
	
	auto end = chrono::high_resolution_clock::now();
	double simulation_time= chrono::duration_cast<chrono::microseconds>(end - start).count();
	double init_time= chrono::duration_cast<chrono::microseconds>(end - s_init).count();
	double init2_time= chrono::duration_cast<chrono::microseconds>(end - s_init2).count();
 printf("----------------Test GPU: Simulation v1 Time--------------\n");
 printf("Execution Time = %.2f [s] \n", (simulation_time/1000000));
 printf("Init Time  = %f [us] \n", (init_time));
 printf("Init2 Time  = %f [us] \n", (init2_time));
 //printf("Init Time  = %.3f [ms] \n", (allocate_time/1000.0));

  return 0;
}

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
