/**
 * @file
 * @brief implements mandatory user class PrimaryGeneratorAction
 */

#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "Randomize.hh"
#include <cmath>
using namespace CLHEP;

PrimaryGeneratorAction::PrimaryGeneratorAction()
{
  G4int n_particle = 1;
  position = G4ThreeVector();
  gun = new G4ParticleGun(n_particle);
  particleGun = new G4GeneralParticleSource ();
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete gun;
  delete particleGun;
}

///////////////////////////////////////////////////////
///// ********  General Particle Source  ******** /////
///////////////////////////////////////////////////////

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  particleGun->GeneratePrimaryVertex(anEvent);
}

