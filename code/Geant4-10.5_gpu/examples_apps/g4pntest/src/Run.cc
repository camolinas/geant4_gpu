#include "Run.hh"
#include "AnalysisManager.hh"

#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Run::Run(const std::vector<G4String> mfdName) :
  G4Run(),
  Vol_ID(-1)
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
Run::~Run()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void Run::RecordEvent(const G4Event* aEvent)
{
  G4int evtNb = aEvent->GetEventID();
  auto analysisManager = G4AnalysisManager::Instance();
  if ( Vol_ID == -1 )
    {
      Vol_ID = G4SDManager::GetSDMpointer()->GetCollectionID("MFD_test/EDep");
    }
  ////////////////////////////////////////
  // **** Multifunctional detector **** //
  ////////////////////////////////////////

  G4HCofThisEvent* HCE = aEvent->GetHCofThisEvent();
  if(!HCE) return;
  
  G4THitsMap<G4double>* evtMap = 
    static_cast<G4THitsMap<G4double>*>(HCE->GetHC(Vol_ID));
  std::map<G4int,G4double*>::iterator itr;
  for (itr = evtMap->GetMap()->begin(); itr != evtMap->GetMap()->end(); itr++) {
    G4double edep = *(itr->second);
    G4int copyNb  = (itr->first);

//    G4cout<<"Entra "<<edep/keV<<G4endl;
    analysisManager->FillH1(0, edep/keV);
    
  }
  G4Run::RecordEvent(aEvent);
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void Run::Merge(const G4Run * aRun) {
  const Run * localRun = static_cast<const Run *>(aRun);
  G4Run::Merge(aRun);
}

