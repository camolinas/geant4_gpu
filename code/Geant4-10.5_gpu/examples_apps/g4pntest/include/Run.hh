/// \file include/Run.hh
/// @Date July 2020
/// @Author DFlechas, dcflechasg@unal.edu.co

#ifndef Run_h
#define Run_h 1

#include "G4Run.hh"
#include "G4Event.hh"

#include "G4THitsMap.hh"
#include <vector>
class Run : public G4Run {

public:
  Run(const std::vector<G4String> mfdName);
  virtual ~Run();

public:
  virtual void RecordEvent(const G4Event*);
  virtual void Merge(const G4Run*);

private:
  G4int Vol_ID; 
};

//

#endif
