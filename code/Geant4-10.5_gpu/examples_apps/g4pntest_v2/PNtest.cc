//
// $Id: PNtest.cc July 2020 $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "SetupConstruction.hh"
#include "ActionInitialization.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif


#include "Randomize.hh"

#include "G4EventManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

#include "QGSP_BERT_HP.hh"
#include "G4PhysListFactory.hh"
#include "G4StepLimiterPhysics.hh"
#include "G4VModularPhysicsList.hh"
#include "G4VUserActionInitialization.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include <chrono>
using namespace std;
 
int main(int argc,char** argv) {

auto start = chrono::high_resolution_clock::now();
  // Detect interactive mode (if no arguments) and define UI session
  G4UIExecutive* ui = 0;
  if ( argc == 1 ) {
    ui = new G4UIExecutive(argc, argv);
  }
  
  //choose the Random engine
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  //set random seed with system time
  G4long seed = time(NULL);
  G4Random::setTheSeed(seed);
  
  // G4MULTITHREADED Construct the default run manager
/* #ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
  runManager->SetNumberOfThreads(2); // Is equal to 2 by default
#else */
  G4RunManager* runManager = new G4RunManager;
//#endif
  
  // set mandatory initialization classes
  runManager->SetUserInitialization(new SetupConstruction);
  G4VModularPhysicsList* physicsList = new QGSP_BERT_HP;
 
  physicsList->RegisterPhysics(new G4StepLimiterPhysics());
  runManager->SetUserInitialization(physicsList);

  runManager->SetUserInitialization(new ActionInitialization());

auto s_init = chrono::high_resolution_clock::now();
    // Visualization manager construction
  auto visManager = new G4VisExecutive;
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  auto UImanager = G4UImanager::GetUIpointer();
  
  if ( !ui ) {
    // execute an argument macro file if exist
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  }
  else {
    UImanager->ApplyCommand("/run/initialize");
    UImanager->ApplyCommand("/control/execute macros/vis.mac");
    // start interactive session
    ui->SessionStart();
    delete ui;
  }

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted 
  // in the main() program !

  delete visManager;
  delete runManager;

 auto end = chrono::high_resolution_clock::now();
 double simulation_time= chrono::duration_cast<chrono::microseconds>(end - start).count();
 double init_time= chrono::duration_cast<chrono::microseconds>(end - s_init).count();
 printf("----------------Test GPU: Simulation v2 Time--------------\n");
 printf("Execution Time = %.2f [s] \n", (simulation_time/1000000));
 printf("Init Time  = %.2f [s] \n", (init_time/1000000));


}

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
