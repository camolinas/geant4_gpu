#ifndef SetupConstruction_h
#define SetupConstruction_h 1

/*! @file SetupConstruction.hh
  @brief Defines mandatory user class SetupConstruction.
  @date july, 2020
  @author D.Flechas dcflechasg@unal.edu.co
  @version 1.0
  
  In this header file, the 'physical' setup is defined: materials, geometries and positions. 
  This class defines the experimental hall used in the toolkit.
*/

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4ThreeVector.hh"

class G4LogicalVolume;
class G4Region;
class G4Material;

class SetupConstruction : public G4VUserDetectorConstruction
{
public:
  //! Constructor
  SetupConstruction();
  //! Destructor
  ~SetupConstruction();
public:
  //! Construct geometry of the setup
  virtual G4VPhysicalVolume* Construct();
  //! Declare the volumes already constructed as 'sensitive'
  virtual void ConstructSDandField();
  //! Update geometry
  void UpdateGeometry();

private:
  void ConstructSetupTest(G4bool vis_flag=true,G4bool solid_flag=false,G4bool Xray_flag=true);
private:
  G4VPhysicalVolume* physiWorld;
  G4LogicalVolume* logicWorld ;
  G4bool constructed;
};

#endif // end SetupConstruction_h
