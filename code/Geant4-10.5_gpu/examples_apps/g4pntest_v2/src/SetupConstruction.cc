/*! @file SetupConstruction.cc
  @brief Defines mandatory user class SetupConstruction.
  @author Pico (L.G. Sarmiento lgsarmientop@unal.edu.co)
  @author Flechas (D. Flechas dcflechasg@unal.edu.co)
  In this source file, the 'physical' setup is defined: materials, geometries and positions. 
  This class defines the experimental hall used in the toolkit.
*/

/* no-geant4 classes*/
#include "SetupConstruction.hh"

/*  units  */
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

/* geometric objects */
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

/* geant4 materials */
#include "G4NistManager.hh"
#include "G4Material.hh"
//#include "G4Element.hh"
//#include "G4Isotope.hh"

/* solid volumes */
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polycone.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4AssemblyVolume.hh"

/* logic and physical volume */
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"

/* optical process in boundaries */
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4OpticalSurface.hh"

/*sensitive volumes */
#include "G4SDManager.hh"

#include "G4PSEnergyDeposit.hh"
#include "G4PSNofStep.hh"
#include "G4PSCellFlux.hh"
#include "G4PSPassageCellFlux.hh"
#include "G4PSFlatSurfaceFlux.hh"
#include "G4PSFlatSurfaceCurrent.hh"

#include "G4SDParticleWithEnergyFilter.hh"
#include "G4SDParticleFilter.hh"
#include "G4SDChargedFilter.hh"

/* for UpdateGeometry */
#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif
#include "G4RegionStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

/* visualization */
#include "G4VisAttributes.hh"


SetupConstruction::SetupConstruction()
{
  /* flags */
  constructed = false;
}

SetupConstruction::~SetupConstruction()
{
}

/*!
  This function is called by G4 when the detector has to be created\n
  Definitions of Solids, Logical Volumes and Physical Volumes comes here.
  //! \sa ConstructEnclosure(), ConstructIMPLANT(), ConstructSiDetectors(), ConstructGeDetectors() and ConstructSDandField()
  */

G4VPhysicalVolume* SetupConstruction::Construct()
{
  //------------------------------
  // World
  //------------------------------
  G4double halfWorldLength = 10.0* m;

  G4Box * solidWorld= new G4Box("world",halfWorldLength,halfWorldLength,halfWorldLength);
  logicWorld = new G4LogicalVolume(solidWorld,
				   G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR"),
				   "World", 0, 0, 0);
  logicWorld -> SetVisAttributes(G4VisAttributes::GetInvisible()); //never show

  //! \li Must place the \ref logicWorld "World" physical volume unrotated at \f$(0,0,0)\f$.
  physiWorld = new G4PVPlacement(0,               // no rotation
				 G4ThreeVector(), // at (0,0,0)
				 logicWorld,      // its logical volume
				 "World",         // its name
				 0,               // its mother  volume
				 false,           // no boolean operations
				 0);              // copy number


  /*************************************/
  /*****   REAL DEAL FROM HERE ON  *****/
  /*************************************/
  G4bool vis_flag,solid_flag,Xray_flag;
  ConstructSetupTest(vis_flag=true, solid_flag=false, Xray_flag=false);
  
  /** In this routine the logical volumes to score get register with
      the SDmanager **/
  constructed = true;
  
  //! \li Always return the physical World
  return physiWorld;
}

void SetupConstruction::ConstructSetupTest(G4bool vis_flag,G4bool solid_flag,G4bool Xray_flag)
{
  //////----------------------------------------------------------///////
  //////----------- Acrilyc_Capsule :: Source container ----------///////
  //////-----------------------------------------------------------///////
  
  G4VisAttributes* red_vis 
    = new G4VisAttributes(vis_flag,G4Colour::Red());
  red_vis->SetForceSolid(solid_flag);
  red_vis->SetForceWireframe(Xray_flag);

  //--------------------------------------------------
  // Sensible gas 3He detectors: 75% 3He + 25% Ar
  //--------------------------------------------------
  G4double density = 0.00079*g/cm3;  
  G4double temperature = 298*kelvin;
  G4double pressure = 5.0*atmosphere;
  G4Isotope* He3 = new G4Isotope("he3",2.,3,3.02*g/mole);
  G4Element* elHe3 = new G4Element("Helium3","He3", 1);
  elHe3->AddIsotope(He3,100.*perCent);
  G4Material* He3Ar;
  He3Ar =  new G4Material("He3Ar",density,2);//,kStateGas,temperature,pressure);
  He3Ar->AddElement(elHe3, 0.75);
  He3Ar->AddElement(G4NistManager::Instance()->FindOrBuildElement("Ar"), 0.25);

  
  G4RotationMatrix* Rotation 
    = new G4RotationMatrix();
  Rotation->rotateX(-M_PI/2.);
  G4double Length = 5.0*cm; // length
  G4double Radius = 5.0*cm/2.0; // radius
  const G4int  numZPlanes = 2;
  G4double zPlane[numZPlanes] = {0.0*cm,Length};
  G4double rInner[numZPlanes] = {0.0*cm,0.0*cm};
  G4double rOuter[numZPlanes] = {Radius,Radius};
  
  G4Polycone* sol_CrystalTest;
  sol_CrystalTest = new G4Polycone("Sol_test",0.*rad,
				     2*M_PI*rad,numZPlanes,zPlane,rInner,rOuter);
  
  G4LogicalVolume* log_CrystalTest;
  log_CrystalTest = new G4LogicalVolume(sol_CrystalTest, He3Ar,"Log_test",0,0,0);
  log_CrystalTest->SetVisAttributes(red_vis);
  
  G4VPhysicalVolume* phy_CrystalTest;
  phy_CrystalTest = new G4PVPlacement(Rotation,         // no rotation
				      G4ThreeVector(0.0,Length+2*cm,0.0), //Completa
				      log_CrystalTest,    // its logical volume
				      "Phy_test",      // its name
				      logicWorld,
				      false,     // no boolean operations 
				      0,
				      true);

  //--------------------------------------------------
  // FarmSoil
  //--------------------------------------------------
  G4double rho_dry = 0.9*g/cm3; //// farm soil dry, master thesis Martha Liliana
  ///-- FarmSoil (to agree with "XRF-209-09", sample name: "XRF_1521 M1 Tierra- Cristancho U.N.", date "4-12-9") --///
	
  G4Material* FarmSoil = new G4Material("FarmSoil", rho_dry ,25);//se debe revisar la densidad
	
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_SILICON_DIOXIDE"), 60.57*perCent);  
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_ALUMINUM_OXIDE"), 12.89*perCent);  
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_FERRIC_OXIDE"), 2.40*perCent);
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_CALCIUM_OXIDE"),  1.54*perCent);
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE"),  0.69*perCent);
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE"), 0.57*perCent);

  G4Material* PHOSPHORUS_PENTOXIDE = new G4Material("PHOSPHORUS_PENTOXIDE", 2.39*g/cm3 , 2);
  PHOSPHORUS_PENTOXIDE->AddElement((G4NistManager::Instance())->FindOrBuildElement("O"), 5);
  PHOSPHORUS_PENTOXIDE->AddElement((G4NistManager::Instance())->FindOrBuildElement("P"), 2);

  FarmSoil->AddMaterial(PHOSPHORUS_PENTOXIDE, 0.51*perCent); // See Sand material definition

  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_POTASSIUM_OXIDE"), 0.49*perCent); 
  FarmSoil->AddMaterial((G4NistManager::Instance())->FindOrBuildMaterial("G4_SODIUM_MONOXIDE"), 0.55*perCent);
	
  G4Material* MANGANESE_OXIDE = new G4Material("MANGANESE_OXIDE", 5.37*g/cm3 , 2);
  MANGANESE_OXIDE->AddElement((G4NistManager::Instance())->FindOrBuildElement("O"), 1);
  MANGANESE_OXIDE->AddElement((G4NistManager::Instance())->FindOrBuildElement("Mn"), 1);
	
  FarmSoil->AddMaterial(MANGANESE_OXIDE, 0.04*perCent);
  //// ppm concentration was corrected + organic material added
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Ba"), 0.0476*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("S"),  0.0244*perCent);  
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Zr"), 0.0191*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Sr"), 0.0162*perCent);  
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("V"),  0.0118*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Zn"), 0.0116*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Cr"), 0.0086*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Pb"), 0.0044*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Cu"), 0.0031*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Rb"), 0.0023*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("Ni"), 0.0020*perCent);
  // Organic material: 19.599 % (H:5%,C:52%,N:3%,O:40%)
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("H"), 0.97994*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("C"), 10.191*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("N"), 0.58797*perCent);
  FarmSoil->AddElement((G4NistManager::Instance())->FindOrBuildElement("O"), 7.8396*perCent);

  ////////////
  /// Soil ///
  ////////////
  G4double Soil_length = 4.0*m;
  
  G4Box* Sol_Soil = new G4Box("S_Soil",Soil_length/2.0,Soil_length/2.0,Soil_length/2.0);
  G4LogicalVolume* Log_Soil = new G4LogicalVolume(Sol_Soil,
						  FarmSoil,
						  "L_Soil",0,0,0);
  G4VPhysicalVolume* Phy_Soil  = new G4PVPlacement( Rotation,
						   G4ThreeVector(0,-Soil_length/2.0,0),
						   Log_Soil,
						   "P_Soil",
						   logicWorld,
						   false,
						   0,
						   true);
  
}


void SetupConstruction::ConstructSDandField()
{
  //================================================
  // Sensitive detectors : MultiFunctionalDetector
  //================================================
  //  Sensitive Detector Manager.
  G4SDManager* pSDman = G4SDManager::GetSDMpointer();
  G4MultiFunctionalDetector* mFDet
     = new G4MultiFunctionalDetector("MFD_test");
  pSDman->AddNewDetector( mFDet );  
  G4PSEnergyDeposit* scorer0 = new G4PSEnergyDeposit("EDep");
  mFDet->RegisterPrimitive(scorer0);
  SetSensitiveDetector("Log_test",mFDet);
}

//! from the detector messenger you can force a geometry re-computation
void SetupConstruction::UpdateGeometry()
{
  if(physiWorld) 
    G4RunManager::GetRunManager()->DefineWorldVolume(Construct());
}
