#include "RunAction.hh"
#include "Run.hh"
#include "AnalysisManager.hh"

//-- In order to obtain detector information.
#include "G4RunManager.hh"
#include "G4THitsMap.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"    

#include <fstream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// Constructor
RunAction::RunAction()
  : G4UserRunAction()
{
  //   vector represents a list of MultiFunctionalDetector names.
  fSDName.push_back(G4String("pn_test"));
  
   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// Destructor.
RunAction::~RunAction()
{
 fSDName.clear();

 delete G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//==
void RunAction::BeginOfRunAction(const G4Run* aRun)
{
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);

  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetVerboseLevel(2);
  analysisManager->SetFileName("out_test");

  analysisManager->OpenFile();
  analysisManager->CreateH1("Edep","Energy (keV)", 1000, 0., 1000.); // Id = 0
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4Run* RunAction::GenerateRun()
{
  return new Run(fSDName);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//== 
void RunAction::EndOfRunAction(const G4Run* aRun)
{
  //if(!IsMaster()) return;
  //G4int nofEvents = aRun->GetNumberOfEvent();
  //if (nofEvents == 0) return;

  auto analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();
}
//
// --
