#include "G4ParticleHPDataPoint_CUDA.hh"
#include "G4InterpolationScheme_CUDA.hh"
//#include "struct_input_CUDA.hh"
//#include "struct_input.hh"
//#include "/home/gfnun/Geant_varieties/Geant4-10.5_gpu_harmo2_v1/src/geant4.10.05.p01/source/processes/hadronic/models/particle_hp/include/struct_input.hh"
  
//void cuda_harmo2(input_data *data_ptr, G4ParticleHPDataPoint *theData_out, int nRanges_a, int nRanges_p, int e_size, int nEntries_a, int nEntries_p, int nEntries_m, int counter, int m_tmp);

void cuda_harmo2(G4ParticleHPDataPoint *theData_a, G4ParticleHPDataPoint *theData_p, double *eAndxsec, bool *select_data, G4InterpolationScheme *scheme_a, G4InterpolationScheme *scheme_p,int *start_a, int nRnages_a, int *start_p, int nRanges_p, int e_size, int nEntries_a, int nEntries_p, G4ParticleHPDataPoint *theData_out, G4ParticleHPDataPoint *theData_merge, int nEntries_m, int counter, int m_tmp);
