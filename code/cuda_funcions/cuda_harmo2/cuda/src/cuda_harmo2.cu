#include <cuda.h>
#include <iostream>
#include <string>
#include <chrono>	

// Geant4 Libraries
#include "globals.hh"
#include "G4ParticleHPDataPoint_CUDA.hh"
#include "G4InterpolationScheme_CUDA.hh"

using namespace std;

__global__ void load_data(G4ParticleHPDataPoint *theData_out, G4ParticleHPDataPoint *theData_apm, double *eAndxsec, double *y_array, int e_size, int m_tmp){
		int idx = blockIdx.x * blockDim.x + threadIdx.x;
		
		if( idx < e_size ){
			
			if( idx < m_tmp ){
//					if( idx == 0) printf("\n idx[0] energy %g \n", theData_merge[ idx ].GetX());
	//				if( idx == 0) printf("\n idx[0] xsec %g \n", theData_merge[ idx ].GetY());
					theData_out[ idx ].SetData( theData_apm[ e_size+idx ].GetX(), theData_apm[ e_size+idx ].GetY());	
			}

			if( idx >= m_tmp ){
		//			if( idx == m_tmp ) printf("idx = tmp_init %d \n", idx);
					if( (idx-m_tmp) < 0) return;
					theData_out[ idx ].SetData( eAndxsec[ idx-m_tmp ], (eAndxsec[ e_size+idx-m_tmp ] + y_array[ idx-m_tmp ]));		
			}
		} // (if) 
}
__global__ void getxsec(G4ParticleHPDataPoint *theData_apm, double *eAndxsec, double *y_array, bool *select_data, G4InterpolationScheme *scheme_a, G4InterpolationScheme *scheme_p, int *start_ap, int nRanges_a, int nRanges_p, int size, int nEntries_a, int nEntries_p, double last_e_p, double last_xsec_p, double last_e_a, double last_xsec_a){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
     	
	if( idx < size ){
//		double e = energy[ idx ];
		double e = eAndxsec[ idx ];
		bool swap_data = select_data[ idx ];
		int index = 0;	
		int it = 0;
		int theScheme = 1;
		extern __shared__ G4InterpolationScheme scheme_shared[];
		G4InterpolationScheme *scheme_shareda = scheme_shared;
		G4InterpolationScheme *scheme_sharedp = (G4InterpolationScheme*)&scheme_shareda[nRanges_a];

/////////////////////////// Select theData ///////////////

		int nEntries = 0;
		double last_e = 0.0;  			
		double last_xsec = 0.0;  		

		scheme_shareda = scheme_a;
		scheme_sharedp = scheme_p;
		__syncthreads(); 

		if( swap_data == false ){					// Take the energy from active object

			nEntries = nEntries_p;
			last_e = last_e_p;							// theData_p[nEntries-1].GetX();
			last_xsec = last_xsec_p;				// theData_p[nEntries-1].GetY();

//////////////////////////////  GetXsec /////////////////

		for( index = 0; index < nEntries_p; index++ ){
			if( theData_apm[ nEntries_a+index ].GetX() >= e ) break;	
			}
		}

	if( swap_data == true ){ 					// Take the energy from passive object

		nEntries = nEntries_a;
		last_e = last_e_a;
		last_xsec = last_xsec_a;

		for( index = 0; index < nEntries_a; index++ ){
			if( theData_apm[ index ].GetX() >= e ) break;	
		}
	}	

//	index_output[ idx ] = index;
	int low = index-1;

  if(index==0){ 
		low = 0;
		index = 1; 										  // high
	}else if( index==nEntries ){
		low = nEntries-2;
		index = nEntries-1; 						// high
	}

/////////////////////////// Select theData ///////////////
// x=e, x1=theData[low].GetX(), x2=theData[high].GetX(), y1=theData[low].GetY(), y2=theData[high].GetY()

	double GetX_high = 0.;  			
	double GetX_low  = 0.;  			
	double GetY_high = 0.;  			
	double GetY_low  = 0.;  			

//	if( theData_a[ idx ].GetX() <= theData_p[ idx ].GetX() )
	if( select_data[ idx ] == false ){                 // Take the energy from active object
			GetX_high = theData_apm[ nEntries_a+index ].GetX();  			
			GetX_low  = theData_apm[ nEntries_a+low ].GetX();  			
			GetY_high = theData_apm[ nEntries_a+index ].GetY();  			
			GetY_low  = theData_apm[ nEntries_a+low ].GetY();  			
  }else{

			GetX_high = theData_apm[ index ].GetX();  			
			GetX_low = theData_apm[ low ].GetX();  			
			GetY_high = theData_apm[ index ].GetY();  			
			GetY_low = theData_apm[ low ].GetY();  			
	}
//////////////////////////////////////////////////////////
/*	if( idx < 6) printf("kernel last_e(1) = %g idx(%d) || ", last_e, idx);
	if( idx == 6) printf("\n");
*/
	if( e < last_e ){    //if(e<theData[nEntries-1].GetX())

	//	if ( theData[high].GetX() !=0
		if( ( GetX_high !=0) && (fabs( (GetX_high-GetX_low)/GetX_high ) < 0.000001) ){
			y_array[ idx ] = GetY_low;
		}else{
	//		y_array[ idx ] = 999.0;
      if( swap_data == false ){                                  
				 it = 0; 
	  	   for( low=1; low<nRanges_p; low++ ){                                                                            
		    	  if(index<start_ap[ nRanges_a+low]) break;                                                    
						it = low;	                                                                 
    		 }                                                                            
    	//theScheme = scheme_p[it];
    	theScheme = scheme_sharedp[it];
			}
      if( swap_data == true ){                                   
				 it = 0;
	  	   for( low=1; low<nRanges_a; low++ ){                                                                            
		    	  if(index<start_ap[low]) break;                                                                                                                     
						it = low;
    		 }                                                                            
//  	 	theScheme = scheme_a[it];
    	theScheme = scheme_shareda[it];
			}

			switch(theScheme){
		     case 1:                                                                       
//       result = LinearLinear(x, x1, x2, y1, y2);  // x=e, x1=theData[low].GetX(), x2=theData[high].GetX(), y1=theData[low].GetY(), y2=theData[high].GetY()
				 				//G4double slope=0, off=0;
			   				if( (GetX_high - GetX_low) == 0 ){
									y_array[ idx ] = (GetY_high + GetY_low)/2.;
									break;
								}
								y_array[ idx ] = e*((GetY_high-GetY_low)/(GetX_high-GetX_low)) + ( GetY_high - GetX_high*((GetY_high-GetY_low)/(GetX_high-GetX_low)) );
								break;
		     case 2:                                                         // result = LinearLinear(x, x1, x2, y1, y2);                                   
			   				if( (GetX_high - GetX_low) == 0 ){
									y_array[ idx ] = (GetY_high + GetY_low)/2.;
									break;
								}
								y_array[ idx ] = e*((GetY_high-GetY_low)/(GetX_high-GetX_low)) + ( GetY_high - GetX_high*((GetY_high-GetY_low)/(GetX_high-GetX_low)) );
								break;
		     case 3:                                                        // result = LinearLogarithmic(x, x1, x2, y1, y2);                              
			   				if( e == 0 ) y_array[ idx ] = GetY_low + GetY_high/2.;  //this should be (x1+x2)/2?
			   				else if(GetX_low == 0) y_array[ idx ] = GetY_low;
			   				else if(GetX_high == 0) y_array[ idx ] = GetY_high;
			   				else{ 	                                                //result = LinearLinear(G4Log(x), G4Log(x1), G4Log(x2), y1, y2);
			   				if( (log(GetX_high) - log(GetX_low)) == 0 ){
									y_array[ idx ] = (GetY_high + GetY_low)/2.;
								break;
								}
				y_array[ idx ] = log(e)*((GetY_high-GetY_low)/(log(GetX_high)-log(GetX_low))) + ( GetY_high-log(GetX_high)*((GetY_high-GetY_low)/(log(GetX_high)-log(GetX_low))) );
								}
		       			break;
		     case 4:                                                        // result = LogarithmicLinear(x, x1, x2, y1, y2);
						   if( GetY_low == 0 || GetY_high == 0 ) y_array[ idx ] = 0;                                                    
						   else{                                                    // result = LinearLinear(x, x1, x2, G4Log(y1), G4Log(y2));                       
			   			 		if( (GetX_high - GetX_low) == 0 ){
											y_array[ idx ] = exp(( log(GetY_high) + log(GetY_low) )/2.);
											break;
									}		
			 y_array[ idx ] = exp( e*((log(GetY_high)-log(GetY_low))/(GetX_high-GetX_low)) + ( GetY_high - GetX_high*((log(GetY_high)-log(GetY_low))/(GetX_high-GetX_low)) ));
							 }
						   break;                                                                      
		     case 5:                                                        // result = LogarithmicLogarithmic(x, x1, x2, y1, y2);                         
						   if( e == 0 ) y_array[ idx ] = GetY_low + GetY_high/2.;
						   else if( GetX_low == 0 ) y_array[ idx ] = GetY_low;                                                       
						   else if( GetX_high == 0 ) y_array[ idx ] = GetY_high;                                                       
			
							 if( (GetY_low == 0) || (GetY_high == 0) ) y_array[ idx ] = 0;                                                    
						   else{                                                    // result = LinearLinear(G4Log(x), G4Log(x1), G4Log(x2), G4Log(y1), G4Log(y2));  
			   					if( (log(GetX_high) - log(GetX_low)) == 0 ){
										y_array[ idx ] = exp( (log(GetY_high)+log(GetY_low))/2. );
										break;
									}
	y_array[ idx ] = exp( log(e)*((log(GetY_high)-log(GetY_low))/(log(GetX_high)-log(GetX_low))) + ( log(GetY_high)-log(GetX_high)*((log(GetY_high)-log(GetY_low))/(log(GetX_high)-log(GetX_low))) ));
									break;
							}
			        break;                                                                      
		     case 6:                                                       //  result = Random(x, x1, x2, y1, y2);                                         
	       				y_array[ idx ] = 11111111.;
				 				break;                                                                      
		     default:
	       				y_array[ idx ] = 22222222.;
//						     if( idx < 6 ) printf("theScheme default =  %d \n", theScheme);                                
//       throw G4HadronicException(__FILE__, __LINE__, "G4ParticleHPInterpolator::Carthesian Invalid InterpolationScheme");
				     		 break;
				 } // end switch
		} // else into e<last_e
	}else{

			y_array[ idx ] = last_xsec;   //theData_x[nEntries-1].GetY();  		
	} 
 } // threads
}


void cuda_harmo2(G4ParticleHPDataPoint *theData_a, G4ParticleHPDataPoint *theData_p, double *eAndxsec, bool *select_data, G4InterpolationScheme *scheme_a, G4InterpolationScheme *scheme_p,int *start_a, int nRanges_a, int *start_p, int nRanges_p, int e_size, int nEntries_a, int nEntries_p, G4ParticleHPDataPoint *theData_out, G4ParticleHPDataPoint *theData_merge, int nEntries_m, int counter, int m_tmp){
//void cuda_harmo2(G4ParticleHPDataPoint *theData_apm, double *eAndxsec, bool *select_data, G4InterpolationScheme *scheme_a, G4InterpolationScheme *scheme_p,int *start_ap, int nRanges_a, int nRanges_p, int e_size, int nEntries_a, int nEntries_p, G4ParticleHPDataPoint *theData_out, int nEntries_m, int counter, int m_tmp){

//printf("---------------- CUDA Function Time --------------\n");
//auto start = chrono::high_resolution_clock::now();

	// CPU variables
//	G4ParticleHPDataPoint *theData_out = new G4ParticleHPDataPoint[e_size];
//	G4ParticleHPDataPoint *theData_outHost;
	int size = counter + 1;
	G4ParticleHPDataPoint *theData_apm;
	int *start_ap;
	
	// Device variables
//	G4ParticleHPDataPoint *d_theData_a;
//	G4ParticleHPDataPoint *d_theData_p;
//	G4ParticleHPDataPoint *d_theData_merge;
	G4ParticleHPDataPoint *d_theData_apm;
	G4ParticleHPDataPoint *d_theData_out;
	G4InterpolationScheme *d_scheme_a;
	G4InterpolationScheme *d_scheme_p;
//	double *d_energy;
	double *d_eAndxsec;
	double *d_y_array;
//	double *d_xsec_array;
//	int *d_start_a;
//	int *d_start_p;
	int *d_start_ap;
	bool *d_select_data;
	
	// Allocate Host memory
//	cudaError_t status = cudaMallocHost((void**)&theData_outHost, sizeof(G4ParticleHPDataPoint) * e_size);
//	if( status != cudaSuccess ) printf("Error Allocating pinned memory \n"); 
	cudaError_t status = cudaMallocHost((void**)&theData_apm, sizeof(G4ParticleHPDataPoint)*(e_size+nEntries_m));
	if( status != cudaSuccess ) printf("Error Allocating apm in pinned memory \n"); 
	status = cudaMallocHost((void**)&start_ap , sizeof(int)*(nRanges_a+nRanges_p));
	if( status != cudaSuccess ) printf("Error Allocating start in pinned memory \n"); 

	// Initilize Host variables
    for( int i = 0; i<nEntries_a; i++) theData_apm[i] = theData_a[i];
    for( int i = 0; i<nEntries_p; i++) theData_apm[i+nEntries_a] = theData_p[i];
    for( int i = 0; i<nEntries_m; i++) theData_apm[i+e_size] = theData_merge[i];
 
    for(int i = 0; i<nRanges_a; i++) start_ap[i] = start_a[i];
    for(int i = 0; i<nRanges_p; i++) start_ap[i+nRanges_a] = start_p[i];        

	// Allocate Device memory
//	cudaMalloc((void**)&d_theData_a, sizeof(G4ParticleHPDataPoint) * nEntries_a);
//	cudaMalloc((void**)&d_theData_p, sizeof(G4ParticleHPDataPoint) * nEntries_p);
	cudaMalloc((void**)&d_theData_apm, sizeof(G4ParticleHPDataPoint) * (e_size+nEntries_m));
//	cudaMalloc((void**)&d_theData_merge, sizeof(G4ParticleHPDataPoint) * nEntries_m);
	cudaMalloc((void**)&d_theData_out, sizeof(G4ParticleHPDataPoint) * e_size);
	cudaMalloc((void**)&d_scheme_a, sizeof(G4InterpolationScheme) * nRanges_a);
	cudaMalloc((void**)&d_scheme_p, sizeof(G4InterpolationScheme) * nRanges_p);
//	cudaMalloc((void**)&d_start_a, sizeof(int) * nRanges_a);
//	cudaMalloc((void**)&d_start_p, sizeof(int) * nRanges_p);
	cudaMalloc((void**)&d_start_ap, sizeof(int) * (nRanges_p+nRanges_a));
//	cudaMalloc((void**)&d_energy, sizeof(double) * size);
	cudaMalloc((void**)&d_eAndxsec, sizeof(double) *2*e_size);
	cudaMalloc((void**)&d_y_array, sizeof(double) * size);
	cudaMalloc((void**)&d_select_data, sizeof(bool) * size);
//	cudaMalloc((void**)&d_xsec_array, sizeof(double) * size);
/*
auto end = chrono::high_resolution_clock::now();
double time= chrono::duration_cast<chrono::microseconds>(end - start).count();
printf("Time: Allocate memory = %.2f [us] \n", time);
	*/

	// Transfer Data To device From host
	cudaMemset(d_y_array, 1, sizeof(double) * size);
//	cudaMemcpy(d_theData_a, theData_a, sizeof(G4ParticleHPDataPoint)*nEntries_a, cudaMemcpyHostToDevice);
//	cudaMemcpy(d_theData_p, theData_p, sizeof(G4ParticleHPDataPoint)*nEntries_p, cudaMemcpyHostToDevice);
//	cudaMemcpy(d_theData_merge, theData_merge, sizeof(G4ParticleHPDataPoint)*nEntries_m, cudaMemcpyHostToDevice);
	cudaMemcpy(d_theData_apm, theData_apm, sizeof(G4ParticleHPDataPoint)*(e_size+nEntries_m), cudaMemcpyHostToDevice);
	cudaMemcpy(d_scheme_a, scheme_a, sizeof(G4InterpolationScheme)*nRanges_a, cudaMemcpyHostToDevice);
	cudaMemcpy(d_scheme_p, scheme_p, sizeof(G4InterpolationScheme)*nRanges_p, cudaMemcpyHostToDevice);
//	cudaMemcpy(d_start_a, start_a, sizeof(int)*nRanges_a, cudaMemcpyHostToDevice);
//	cudaMemcpy(d_start_p, start_p, sizeof(int)*nRanges_p, cudaMemcpyHostToDevice);
	cudaMemcpy(d_start_ap, start_ap, sizeof(int)*(nRanges_a+nRanges_p), cudaMemcpyHostToDevice);
//	cudaMemcpy(d_energy, energy, sizeof(double)* size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_eAndxsec, eAndxsec, sizeof(double)*2*e_size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_select_data, select_data, sizeof(bool) * size, cudaMemcpyHostToDevice);
//	cudaMemcpy(d_xsec_array, xsec_array, sizeof(double) * size, cudaMemcpyHostToDevice);

	double last_e_p = theData_p[nEntries_p-1].GetX();  			
	double last_xsec_p = theData_p[nEntries_p-1].GetY();  			
	double last_e_a = theData_a[nEntries_a-1].GetX();  			
	double last_xsec_a = theData_a[nEntries_a-1].GetY();  			
/*
start = chrono::high_resolution_clock::now();
time= chrono::duration_cast<chrono::microseconds>(start - end).count();
printf("Time: Transfer Data (H->D) = %.2f [us] \n", time);
*/
	// Execute Kernel

	int threads_block = 1024;
	int num_blocks_get =  (int)ceil( size/threads_block );
	if( num_blocks_get == 0 ){
		num_blocks_get = 1;
		threads_block = size;
	}else{
		num_blocks_get = num_blocks_get + 1;
	}
	int num_blocks =  (int)ceil( e_size/threads_block );
	if( num_blocks == 0 ){
		num_blocks = 1;
		threads_block = e_size;
	}else{
		num_blocks = num_blocks + 1;
	}

//	getxsec<<<num_blocks_get, threads_block, nRanges_a*sizeof(G4InterpolationScheme)+nRanges_p*sizeof(G4InterpolationScheme)>>>(d_theData_a, d_theData_p, d_eAndxsec, d_y_array, d_select_data, d_scheme_a, d_scheme_p, d_start_a, nRanges_a, d_start_p, nRanges_p, size, nEntries_a, nEntries_p, last_e_p, last_xsec_p, last_e_a, last_xsec_a);
	getxsec<<<num_blocks_get, threads_block, nRanges_a*sizeof(G4InterpolationScheme)+nRanges_p*sizeof(G4InterpolationScheme)>>>(d_theData_apm, d_eAndxsec, d_y_array, d_select_data, d_scheme_a, d_scheme_p, d_start_ap, nRanges_a, nRanges_p, size, nEntries_a, nEntries_p, last_e_p, last_xsec_p, last_e_a, last_xsec_a);
	
	load_data<<<num_blocks, threads_block>>>(d_theData_out, d_theData_apm, d_eAndxsec, d_y_array, e_size, m_tmp);
//	load_data<<<num_blocks, threads_block>>>(d_theData_out, d_theData_merge, d_eAndxsec, d_y_array, e_size, m_tmp);
/*
end = chrono::high_resolution_clock::now();
time= chrono::duration_cast<chrono::microseconds>(end-start).count();
printf("Time: Kernel execution = %.2f [us] \n", time);
*/
	// Transfer Data To host From device
//	cudaMemcpy(theData_outHost, d_theData_out, sizeof(G4ParticleHPDataPoint) * e_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(theData_out, d_theData_out, sizeof(G4ParticleHPDataPoint) * e_size, cudaMemcpyDeviceToHost);
/*
start = chrono::high_resolution_clock::now();
time= chrono::duration_cast<chrono::microseconds>(start - end).count();
printf("Time: Transfer Data (D->H) = %.2f [us] \n", time);
printf("---------------- CUDA Function Time --------------\n");
*/

//	for( int c = 0; c < (counter+m_tmp); c++)	theData_out[c] = theData_outHost[c];


	// Verification
//G4ParticleHPDataPoint *theData_outtt = theData_out;

//printf("---------------- CUDA Function init --------------\n");
/*  for( int g = 0; g<(counter+m_tmp); g++ ){
    if( (theData_merge[g].GetX() - theData_out[g].GetX()) > 1e-5){
      std::cout << "Error energy " << g << " | merge = " << theData_merge[g].GetX() << " | out = " << theData_out[g].GetX() << std::endl;
    }
    if( (theData_merge[g].GetY() - theData_out[g].GetY()) > 1e-5){
      std::cout << "Error xsec " << g << " | merge = " << theData_merge[g].GetY() << " | out = " << theData_out[g].GetY() << std::endl;
    }
  }
*/
//printf("---------------- CUDA Function close --------------\n");
//	cudaFreeHost(theData_outHost);
//	cudaFree(d_theData_a);
//	cudaFree(d_theData_p);
//	cudaFree(d_theData_merge);
	cudaFreeHost(theData_apm);
	cudaFreeHost(start_ap);
	cudaFree(d_theData_apm);
	cudaFree(d_theData_out);
//	cudaFree(d_energy);
	cudaFree(d_eAndxsec);
	cudaFree(d_y_array);
	cudaFree(d_select_data);
	cudaFree(d_scheme_a);
	cudaFree(d_scheme_p);
	cudaFree(d_start_ap);
//	cudaFree(d_start_a);
//	cudaFree(d_start_p);
//	cudaFree(d_xsec_array);

//	free(theData_out);
//	delete [] theData_out;

//	return theData_outtt;
}
