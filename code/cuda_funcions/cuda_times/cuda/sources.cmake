#------------------------------------------------------------------------------
# sources.cmake
# Module : G4cuda
# Package: Geant4.src.G4externals.G4cuda
#
# Sources description for a library.
# Lists the sources and headers of the code explicitely.
# Lists include paths needed.
# Lists the internal granular and global dependencies of the library.
# Source specific properties should be added at the end.
#
# Created on : 16/07/2020
#
# $Date$     16/07/2020 
# $Revision$ v.0
# $Author$   Dorfell Parra
#
#------------------------------------------------------------------------------


configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/include/CUDA_lib.hh
                ${CMAKE_CURRENT_BINARY_DIR}/include/CUDA_lib.hh )

#
# Include the Geant4 Module.
#
include(Geant4MacroDefineModule)

# List internal includes neeeded
include_directories(${CMAKE_SOURCE_DIR}/source/global/management/include)

#   headers listed under Sources are internal cuda headers
#   Private headers are in src!
include_directories(/usr/local/cuda/include) # CUDA package
include_directories(include)
include_directories(src)

set(CUDA_PUBLIC_HDRS
    ${CMAKE_CURRENT_BINARY_DIR}/include/CUDA_lib.hh 
)
set(CUDA_PRIVATE_HDRS
    cuda_hello.hh
    cuda_fill_nullptr.hh
		cuda_times.hh
		G4ParticleHPDataPoint_CUDA.hh
)
set(CUDA_SRCS
    cuda_hello.cu
    cuda_fill_nullptr.cu
		cuda_times.cu
)
GEANT4_DEFINE_MODULE(NAME G4cuda
    HEADERS
        ${CUDA_PUBLIC_HDRS}
    SOURCES
        ${CUDA_SRCS}
    GRANULAR_DEPENDENCIES
    GLOBAL_DEPENDENCIES
    LINK_LIBRARIES
)
# List any source specific properties here
