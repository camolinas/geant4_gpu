#include <cuda.h>
/*
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <chrono>
*/	

// Geant Libraries
#include "globals.hh"
#include "G4ParticleHPDataPoint_CUDA.hh"


__global__ void scaling(G4ParticleHPDataPoint *theData, int nEntries, double factor){
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
     	
	if( index < nEntries ){
		for( int i = index; i < nEntries; i += stride ){
			theData[i].SetY(theData[i].GetY()*factor);
		}
	} 
}


void cuda_times(G4ParticleHPDataPoint *theData, int nEntries, double factor){

	
	// Device variables
	G4ParticleHPDataPoint *d_theData;
	
	// Allocate Host memory
	// Initilize Host variables
	
	// Allocate Device memory
	cudaMalloc((void**)&d_theData, sizeof(G4ParticleHPDataPoint) * nEntries);
	
	// Transfer Data To device From host
	//cudaMemset(d_index_output, 8, sizeof(int) * nEntries);
	cudaMemcpy(d_theData, theData, sizeof(G4ParticleHPDataPoint)*nEntries, cudaMemcpyHostToDevice);

	// Execute Kernel
	int threads_block = 1024;
	int num_blocks =  (int)ceil( nEntries/threads_block );
	if( num_blocks == 0 ){
		num_blocks = 1;
		threads_block = nEntries;
	}
//	printf("\nNumber of elements = %d \n", nEntries);
//	printf("Threads per Block = %d \n", threads_block);
//	printf("Number of Blocks = %d \n", num_blocks); 

	scaling<<<num_blocks, threads_block>>>(d_theData, nEntries, factor);
	
	// Transfer Data To host From device
	cudaMemcpy(theData, d_theData, sizeof(G4ParticleHPDataPoint)*nEntries, cudaMemcpyDeviceToHost);

	// Verification
	
	cudaFree(d_theData);
}
