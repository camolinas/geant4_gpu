#include <cuda.h>
/*
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <chrono>
*/	

// Geant Libraries
#include "globals.hh"
#include "G4ParticleHPDataPoint_CUDA.hh"


__global__ void threshold(G4ParticleHPDataPoint *theData, int *index_output, double energy, int nEntries){
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
     	
	if( index < nEntries ){
		for( int i = index; i < nEntries; i += stride ){
			if( theData[i].GetX() >= energy) index_output[i] = i;	
		}
	} 
}

__global__ void reduce_min(int *index_output, int *min_index, int nEntries){
	int index = threadIdx.x;
	int stride = blockDim.x;	

	__shared__ int reduction[ 1024 ];
	int offset = 0;

	int regi = 1<<30;
	reduction[ index ] = 1<<30; 
	while( (index + offset) < nEntries ){
		regi = fminf(regi, index_output[ index+offset ]);
		offset += stride;
	}

	reduction[ index ] = regi;
	__syncthreads();	
						     
	int dim = blockDim.x/2;
	while( dim != 0 ){
		if( index < dim ){
			reduction[ index ] = fminf(reduction[ index ], reduction[ index+dim ]);
		}
	__syncthreads();
	dim /= 2;
	}		

	if( index == 0 ){	
		*min_index = reduction[ 0 ];	
	}
}

G4int cuda_getxsec(G4ParticleHPDataPoint *theData, int nEntries, double energy){

	int *index_output;
	int *min_index;
	
	// Device variables
	G4ParticleHPDataPoint *d_theData;
	int *d_index_output;
	int *d_min_index;
	
	// Allocate Host memory
	index_output = (int*)malloc(sizeof(int) * nEntries);
	min_index = (int*)malloc(sizeof(int));
	
	// Initilize Host variables
	*min_index = 0;
	
	// Allocate Device memory
	cudaMalloc((void**)&d_theData, sizeof(G4ParticleHPDataPoint) * nEntries);
	cudaMalloc((void**)&d_index_output, sizeof(int) * nEntries);
	cudaMalloc((void**)&d_min_index, sizeof(int));
	
	// Transfer Data To device From host
	cudaMemset(d_min_index, 0, sizeof(int));
	cudaMemset(d_index_output, 8, sizeof(int) * nEntries);
	cudaMemcpy(d_theData, theData, sizeof(G4ParticleHPDataPoint)*nEntries, cudaMemcpyHostToDevice);

	// Execute Kernel
	int threads_block = 1024;
	int num_blocks =  (int)ceil( nEntries/threads_block );
	if( num_blocks == 0 ){
		num_blocks = 1;
		threads_block = nEntries;
	}
//	printf("\nNumber of elements = %d \n", nEntries);
//	printf("Threads per Block = %d \n", threads_block);
//	printf("Number of Blocks = %d \n", num_blocks); 

	threshold<<<num_blocks, threads_block>>>(d_theData,d_index_output,energy,nEntries);
	reduce_min<<<1, 1024>>>(d_index_output, d_min_index, nEntries);
	
	// Transfer Data To host From device
	cudaMemcpy(min_index, d_min_index, sizeof(int), cudaMemcpyDeviceToHost);

	if( *min_index == 134744064) *min_index = nEntries;												// The case when don't exist mayor e value into the thaData
	// Verification
//	printf("\n Min. index value = %d \n", *min_index);
	G4int minidx = *min_index;
	
	cudaFree(d_theData);
	cudaFree(d_index_output);
	cudaFree(d_min_index);

	free(index_output);
	free(min_index);

	return minidx;
}
