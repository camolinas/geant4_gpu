// -------------------------------------------------------------------
//
// GEANT4 CUDA header file
//
//
// File name:    cuda_hello
//
// Author  Dorfell Parra, 2020/07/21
//
// Modifications:
//
// Description:
// This is a GPU kernel used to print Hello from the CUDA module.
// Description - E

int myfoo_cuda(void);
