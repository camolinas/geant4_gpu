#include <cuda.h>
/*
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <chrono>
*/	

// Geant Libraries
#include "globals.hh"
#include "G4ParticleHPDataPoint_CUDA.hh"


__global__ void load(G4ParticleHPDataPoint *theData_in, G4ParticleHPDataPoint *theData_buff, int nEntries){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;
     	
	if( idx < nEntries ){
		for( int i = idx; i < nEntries; i += stride ){
			theData_buff[i].SetY(theData_in[i].GetY());
			theData_buff[i].SetX(theData_in[i].GetX());
		}
	} 
}


void cuda_check(G4ParticleHPDataPoint *theData_buff, G4ParticleHPDataPoint *theData_in, int nEntries){

	
	// Device variables
	G4ParticleHPDataPoint *d_theData_in;
	G4ParticleHPDataPoint *d_theData_buff;
	
	// Allocate Host memory
	// Initilize Host variables
	
	// Allocate Device memory
	cudaMalloc((void**)&d_theData_in, sizeof(G4ParticleHPDataPoint) * nEntries);
	cudaMalloc((void**)&d_theData_buff, sizeof(G4ParticleHPDataPoint) * nEntries);
	
	// Transfer Data To device From host
	//cudaMemset(d_index_output, 8, sizeof(int) * nEntries);
	cudaMemcpy(d_theData_in, theData_in, sizeof(G4ParticleHPDataPoint)*nEntries, cudaMemcpyHostToDevice);

	// Execute Kernel
	int threads_block = 1024;
	int num_blocks =  (int)ceil( nEntries/threads_block );
	if( num_blocks == 0 ){
		num_blocks = 1;
		threads_block = nEntries;
	}
//	printf("\nNumber of elements = %d \n", nEntries);
//	printf("Threads per Block = %d \n", threads_block);
//	printf("Number of Blocks = %d \n", num_blocks); 

	load<<<num_blocks, threads_block>>>(d_theData_in, d_theData_buff, nEntries);
	
	// Transfer Data To host From device
	cudaMemcpy(theData_buff, d_theData_buff, sizeof(G4ParticleHPDataPoint)*nEntries, cudaMemcpyDeviceToHost);

	// Verification
	
	cudaFree(d_theData_in);
	cudaFree(d_theData_buff);
}
